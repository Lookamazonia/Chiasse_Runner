package lookamazonia.canvaspoc;

import android.os.AsyncTask;

import java.util.Random;

/**
 * Created by Maxime on 29/03/2017.
 */
public class GameThread extends AsyncTask<Float, Integer, Void> {

    private boolean isInAJump = false;
    private boolean isGoingUp = true;
    private boolean isJumpRecycled = true;
    private int peak = 500;

    private CollisionManager collisionManager;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        System.out.println("Process lancé");

    }

    @Override
    protected Void doInBackground(Float... bobyPos) {

        System.out.println("Process en cours");

        collisionManager = new CollisionManager();

        while(true){

            run();

        }

    }

    private void run(){

        boolean updatePlayer = false;

        if(Game.getInstance().isScreenPressed()){

            //New game
            if(Game.getInstance().isGameOver()){
                Game.getInstance().setGameOver(false);
                Game.getInstance().addPlayer();
                Game.getInstance().setScore(0);
            }

            //New jump
            if(!isInAJump && !isGoingUp && isJumpRecycled){
                isGoingUp = true;
                isInAJump = true;
                isJumpRecycled = false;
            }

            peak = 750;

        }else {

            peak = 500;
            if (!isInAJump && !isJumpRecycled) {
                isJumpRecycled = true;
            }

        }

        //Random add obstacles && score increment
        if(!Game.getInstance().isGameOver()){

            Game.getInstance().setScore(Game.getInstance().getScore()+1);

            int rand = (int)(Math.random() * 100000);
            if(rand == 332){
                Game.getInstance().getObstacleFactory().addRandomObstacle();
            }
        }

        //Move obstacles
        ObstacleManager obstacleManager = Game.getInstance().getObstacleManager();
        for(int i = 0; i < obstacleManager.getLesObstacles().size(); i++){
            Obstacle o = obstacleManager.getLesObstacles().get(i);
            if(o.getType().equals("Obstacle")){
                o.setX(o.getX()-0.04);

                if(o.getX() < -300){
                    obstacleManager.getLesObstacles().remove(o);
                    i--;
                }
                if(collisionManager.isCollision(o,obstacleManager.getObstacleByName("Player"))){
                    obstacleManager.getLesObstacles().clear();
                    Game.getInstance().setGameOver(true);
                    break;
                }

            }


        }

        //Jump
        if(isGoingUp){
            if(Game.getPlayerJumpHeight() < peak){
                Game.getInstance().setPlayerJumpHeight(Game.getPlayerJumpHeight()+0.04);
                updatePlayer = true;
            }else{
                isGoingUp = false;
            }
        }else{
            if(Game.getPlayerJumpHeight() > 0){
                Game.getInstance().setPlayerJumpHeight(Game.getPlayerJumpHeight()-0.04);
                updatePlayer = true;
            }else{
                isInAJump = false;
            }
        }

        if(updatePlayer){
            Obstacle o = Game.getInstance().getObstacleManager().getObstacleByName("Player");
            if(o!=null){
                o.setY((int)(600-Game.getInstance().getPlayerJumpHeight()));
            }
        }

    }

    @Override
    protected void onPostExecute(Void result) {
        System.out.println("Process achevé");
    }

}
