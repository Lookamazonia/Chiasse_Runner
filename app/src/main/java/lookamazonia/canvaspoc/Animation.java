package lookamazonia.canvaspoc;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jo on 26/04/2017.
 */

public class Animation {

    private List<Bitmap> frames;
    private int slow;

    public Animation(Resources r,int[] res, int slow){

        frames = new ArrayList<Bitmap>();

        //Load frames
        for(int i = 0; i < res.length; i++){
            Bitmap b = BitmapFactory.decodeResource(r, res[i]);
            frames.add(b);
        }

        this.slow = slow;

    }

    public Bitmap getFrame(int i){

        if(i < frames.size() && i > -1){

            return frames.get(i);

        }

        return null;

    }

    public AnimationInstance getInstance(){

        return new AnimationInstance(this,slow);

    }

    public int getCount(){

        return frames.size();

    }

    public List<Bitmap> getFrames() {
        return frames;
    }

    public void setFrames(List<Bitmap> frames) {
        this.frames = frames;
    }
}
