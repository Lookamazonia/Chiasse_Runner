package lookamazonia.canvaspoc;

/**
 * Created by Maxime on 26/04/2017.
 */
public class CollisionManager {

    public boolean isCollision(Obstacle obs1, Obstacle obs2) {

        if(obs1 != null && obs2 != null){

            if (obs1.getX() < (obs2.getX() + obs2.getW()) && (obs1.getX() + obs1.getW()) > obs2.getX() &&
                    obs1.getY() < (obs2.getY() + obs2.getH()) && (obs1.getY() + obs1.getH()) > obs2.getY()) {

                return true;
            }

        }
        return false;
    }
}
