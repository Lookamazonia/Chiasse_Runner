package lookamazonia.canvaspoc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maxime on 26/04/2017.
 */
public class ObstacleManager {

    private List<Obstacle> lesObstacles;

    public ObstacleManager() {

        lesObstacles = new ArrayList<Obstacle>();

    }

    public void addObstacle(Obstacle o){

        lesObstacles.add(o);

    }

    public Obstacle getObstacleByName(String name){

        for(int i = 0; i < lesObstacles.size(); i ++){

            if(lesObstacles.get(i).getName().equals(name)){

                return lesObstacles.get(i);

            }

        }

        return null;

    }

    public List<Obstacle> getLesObstacles() {
        return lesObstacles;
    }

    public void setLesObstacles(List<Obstacle> lesObstacles) {
        this.lesObstacles = lesObstacles;
    }

}
