package lookamazonia.canvaspoc;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class Main extends AppCompatActivity {

    // gameView will be the view of the game
    // It will also hold the logic of the game
    // and respond to screen touches as well
    GameView gameView;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize gameView and set it as the view
        gameView = new GameView(this);

        setContentView(gameView);
    }

    // GameView class will go here

    // Here is our implementation of GameView
    // It is an inner class.
    // Note how the final closing curly brace }
    // is inside SimpleGameEngine

    // Notice we implement runnable so we have
    // A thread and can override the run method.
    class GameView extends SurfaceView implements Runnable {

        // This is our thread
        Thread gameThread = null;

        // This is new. We need a SurfaceHolder
        // When we use Paint and Canvas in a thread
        // We will see it in action in the draw method soon.
        SurfaceHolder ourHolder;

        // A boolean which we will set and unset
        // when the game is running- or not.
        volatile boolean playing;

        // A Canvas and a Paint object
        Canvas canvas;
        Paint paint;

        long fps;
        private long timeThisFrame;
        boolean isMoving = false;
        float walkSpeedPerSecond = 150;
        public float getBobYPosition() {
            return bobYPosition;
        }
        public void setBobYPosition(float bobYPosition) {
            this.bobYPosition = bobYPosition;
        }
        float bobYPosition = 10;

        public GameView(Context context) {
            super(context);

            ourHolder = getHolder();
            paint = new Paint();
            playing = true;

            Game.getInstance().loadRessources(getResources());

            GameThread testJump = new GameThread();
            testJump.execute();

        }

        @Override
        public void run() {
            while (playing) {

                // Capture the current time in milliseconds in startFrameTime
                long startFrameTime = System.currentTimeMillis();

                // Update the frame
                update();

                // Draw the frame
                draw();

                // Calculate the fps this frame
                // We can then use the result to
                // time animations and more.
                timeThisFrame = System.currentTimeMillis() - startFrameTime;
                if (timeThisFrame > 0) {
                    fps = 1000 / timeThisFrame;
                }

            }

        }

        // Everything that needs to be updated goes in here
        // In later projects we will have dozens (arrays) of objects.
        // We will also do other things like collision detection.
        public void update() {

            // If bob is moving (the player is touching the screen)
            // then move him to the right based on his target speed and the current fps.
                if (isMoving) {
                    bobYPosition = bobYPosition - (walkSpeedPerSecond / fps);
                }
        }

        // Draw the newly updated scene
        public void draw() {

            // Make sure our drawing surface is valid or we crash
            if (ourHolder.getSurface().isValid()) {

                canvas = ourHolder.lockCanvas();
                canvas.drawColor(Color.argb(255, 255, 255, 182));

                ObstacleManager obsMan = Game.getInstance().getObstacleManager();

                for(int i = 0; i < obsMan.getLesObstacles().size(); i++){
                    Obstacle o = obsMan.getLesObstacles().get(i);
                    if(o != null){
                        canvas.drawBitmap(Bitmap.createScaledBitmap(o.getImg().getFrame(),(int)(o.getW()),(int)(o.getH()),true), (int)(o.getX()),(int)(o.getY()), paint);
                    }
                }

                //Game over
                if(Game.getInstance().isGameOver()){
                    paint.setColor(Color.argb(255,  255, 0, 0));
                    paint.setTextSize(200);
                    canvas.drawText("GAME OVER", 650, 500, paint);
                    paint.setColor(Color.argb(255,  0, 0, 0));
                    paint.setTextSize(100);
                    canvas.drawText("Touch screen to start", 700, 800, paint);
                }

                //Score display
                paint.setColor(Color.argb(255,  249, 129, 0));
                paint.setTextSize(100);
                canvas.drawText("SCORE : " + Game.getInstance().getScore(), 30, 90, paint);

                // Draw everything to the screen
                ourHolder.unlockCanvasAndPost(canvas);
            }

        }

        // If SimpleGameEngine Activity is paused/stopped
        // shutdown our thread.
        public void pause() {
            playing = false;
            try {
                gameThread.join();
            } catch (InterruptedException e) {
                Log.e("Error:", "joining thread");
            }

        }

        // If SimpleGameEngine Activity is started then
        // start our thread.
        public void resume() {
            playing = true;
            gameThread = new Thread(this);
            gameThread.start();
        }

        // The SurfaceView class implements onTouchListener
        // So we can override this method and detect screen touches.
        @Override
        public boolean onTouchEvent(MotionEvent motionEvent) {

            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

                // Player has touched the screen
                case MotionEvent.ACTION_DOWN:

                    Game.getInstance().setScreenPressed(true);

                    break;

                // Player has removed finger from screen
                case MotionEvent.ACTION_UP:

                    Game.getInstance().setScreenPressed(false);

                    break;
            }
            return true;
        }

    }
    // This is the end of our GameView inner class

    // More SimpleGameEngine methods will go here

    // This method executes when the player starts the game
    @Override
    protected void onResume() {
        super.onResume();

        // Tell the gameView resume method to execute
        gameView.resume();
    }

    // This method executes when the player quits the game
    @Override
    protected void onPause() {
        super.onPause();

        // Tell the gameView pause method to execute
        gameView.pause();
    }
}
