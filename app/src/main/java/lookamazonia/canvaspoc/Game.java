package lookamazonia.canvaspoc;

import android.content.res.Resources;
import android.graphics.BitmapFactory;

/**
 * Created by Jo on 26/04/2017.
 */

public class Game {

    //Singleton

    private static Game instance = null;
    protected Game() {
        // Exists only to defeat instantiation.
    }
    public static Game getInstance() {
        if(instance == null) {
            instance = new Game();
        }
        return instance;
    }

    private static double playerJumpHeight = 0;
    private boolean screenPressed = false;
    private ObstacleManager obstacleManager = new ObstacleManager();
    private ObstacleFactory obstacleFactory;

    private boolean gameOver = true;
    private long score = 0;

    private Animation playerAnim;
    private Animation chiottesAnim;

    public void loadRessources(Resources r){

        playerAnim = new Animation(r,new int[] {R.drawable.run1,R.drawable.run2,R.drawable.run3,R.drawable.run4},15);
        //chiottesAnim = new Animation(r,new int[] {R.drawable.chiotte1,R.drawable.chiotte2,R.drawable.chiotte3,R.drawable.chiotte4,R.drawable.chiotte5,R.drawable.chiotte6,R.drawable.chiotte7,R.drawable.chiotte8},15);

        obstacleFactory = new ObstacleFactory(obstacleManager,r);

    }

    public void addPlayer(){

        obstacleManager.addObstacle(new Obstacle("Player","Player",200,200,500,500,playerAnim));

    }

    public static double getPlayerJumpHeight() {
        return playerJumpHeight;
    }

    public static void setPlayerJumpHeight(double playerJumpHeight) {
        Game.playerJumpHeight = playerJumpHeight;
    }

    public boolean isScreenPressed() {
        return screenPressed;
    }

    public void setScreenPressed(boolean screenPressed) {
        this.screenPressed = screenPressed;
    }

    public ObstacleManager getObstacleManager() {
        return obstacleManager;
    }

    public void setObstacleManager(ObstacleManager obstacleManager) {
        this.obstacleManager = obstacleManager;
    }

    public Animation getPlayerAnim() {
        return playerAnim;
    }

    public void setPlayerAnim(Animation playerAnim) {
        this.playerAnim = playerAnim;
    }

    public ObstacleFactory getObstacleFactory() {
        return obstacleFactory;
    }

    public void setObstacleFactory(ObstacleFactory obstacleFactory) {
        this.obstacleFactory = obstacleFactory;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }
}
