package lookamazonia.canvaspoc;

import android.graphics.Bitmap;

/**
 * Created by Jo on 26/04/2017.
 */

public class AnimationInstance {

    private Animation source;

    private int currentFrame;

    private int slow;
    private int slowCount;

    public AnimationInstance(Animation a,int slow){

        this.source = a;
        this.slow = slow;

        slowCount = 0;
        currentFrame = 0;

    }

    public Bitmap getFrame(){

        if(slow != -1){

            slowCount++;

            if(slowCount == slow){
                currentFrame++;
                if(currentFrame == source.getCount()){
                    currentFrame = 0;
                }
                slowCount = 0;
            }

        }

        System.out.println("GET FRAME " + currentFrame);

        return source.getFrame(currentFrame);

    }

}
