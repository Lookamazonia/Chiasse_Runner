package lookamazonia.canvaspoc;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;

/**
 * Created by Maxime on 26/04/2017.
 */
public class Obstacle {

    private String name;
    private String type;

    private double x;
    private double y;
    private double w;
    private double h;

    private AnimationInstance img;

    public Obstacle(String name,String type,double x, double y, double w, double h, Animation ain) {
        this.name = name;
        this.type = type;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.img = ain.getInstance();
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getW() {
        return w;
    }

    public void setW(double w) {
        this.w = w;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    public AnimationInstance getImg() {
        return img;
    }

    public void setImg(AnimationInstance img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
