package lookamazonia.canvaspoc;

import android.content.res.Resources;

/**
 * Created by Jo on 26/04/2017.
 */

public class ObstacleFactory {

    private ObstacleManager obstacleManager;

    private Animation PQ;
    private Animation Plunger;

    public ObstacleFactory(ObstacleManager obstacleManager, Resources r){

        this.obstacleManager = obstacleManager;

        PQ = new Animation(r,new int[] {R.drawable.prop2},-1);

        Plunger = new Animation(r,new int[] {R.drawable.prop1},-1);

    }

    public void addRandomObstacle(){

        int randomNum = (int)(1+Math.random()*2);

        if(randomNum==1){
            addPQ();
        }
        if(randomNum==2){
            addPlunger();
        }

    }

    public void addPQ(){

        obstacleManager.addObstacle(new Obstacle("PQ","Obstacle",2300,900,200,200,PQ));

    }

    public void addPlunger(){

        obstacleManager.addObstacle(new Obstacle("Plunger","Obstacle",2300,900,100,200,Plunger));

    }

}
